/*
 * 
        File for use in testing of PCL library used by external predicate.
        
        --------------------------------------------------------------------
        
        Damien Jade Duff

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */      

#include <iostream>

extern bool find_ground_plane();
extern int num_high_points(double xlim_low, double xlim_high, double ylim_low, double ylim_high);

int main(int argc,char** argv){
  find_ground_plane();
  int a;
  a=num_high_points(-0.05,0.05,-0.05,0.05);
  std::cout<<"Num points here is "<<a<<std::endl;
  a=num_high_points(0.2,0.3,-0.05,0.05);
  std::cout<<"Num points here is "<<a<<std::endl;
  a=num_high_points(0.4,0.5,-0.05,0.05);
  std::cout<<"Num points here is "<<a<<std::endl;
  
  a=num_high_points(-0.1,0.1,-0.1,0.1);
  std::cout<<"Num points here is "<<a<<std::endl;
  a=num_high_points(-0.05,0.05,0.2,0.3);
  std::cout<<"Num points here is "<<a<<std::endl;
  a=num_high_points(-0.05,0.05,0.2,0.3);
  std::cout<<"Num points here is "<<a<<std::endl;
}