/*
 * 
        An example of how to do plane extraction;
        adapted from http://pointclouds.org/documentation/tutorials/planar_segmentation.php
        
        --------------------------------------------------------------------
        
        Damien Jade Duff

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */      


#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/pcl_visualizer.h>
std::string scene_filename_;

void
parseCommandLine(int argc, char* argv[])
{

    std::vector<int> filenames;
    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
    if (filenames.size() != 1) {
        std::cout << "Filenames funny.\n";
        exit(-1);
    }

    scene_filename_ = argv[filenames[0]];

}


int
main(int argc, char** argv)
{
    parseCommandLine(argc, argv);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr in_plane(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr not_plane(new pcl::PointCloud<pcl::PointXYZRGB>);

    std::cout << "Loading scene cloud" << std::endl;
    if (pcl::io::loadPCDFile(scene_filename_, *cloud) < 0) {
        std::cout << "Error loading scene cloud." << std::endl;
        return (-1);
    }

    std::cerr << "Point cloud data: " << cloud->points.size() << " points" << std::endl;


    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.01);

    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);


    if (inliers->indices.size() == 0) {
        std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
        return (-1);
    }



    std::cerr << "Model coefficients: " << coefficients->values[0] << " "
              << coefficients->values[1] << " "
              << coefficients->values[2] << " "
              << coefficients->values[3] << std::endl;

    std::cerr << "Model inliers: " << inliers->indices.size() << std::endl;

    pcl::ExtractIndices<pcl::PointXYZRGB> extractor;
    extractor.setInputCloud(cloud);
    extractor.setIndices(inliers);
    extractor.setNegative(false);
    extractor.filter(*in_plane);

    extractor.setNegative(true);
    extractor.filter(*not_plane);

    pcl::visualization::PCLVisualizer viewer("Planar segmentation");
    viewer.setCameraPosition(0.0,-1.0,-2.0,0.0,0.0,1.0);
    
    int viewport_id_0;
    int viewport_id_1;
    int viewport_id_2;
    viewer.createViewPort(0, 0, 0.33, 1.0, viewport_id_0);
    viewer.createViewPort(0.33, 0, 0.66, 1.0, viewport_id_1);
    viewer.createViewPort(0.66, 0, 1.0, 1.0, viewport_id_2);

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> colour_handler_cloud(cloud);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> colour_handler_in_plane(in_plane);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> colour_handler_not_plane(not_plane);

    viewer.addPointCloud(cloud, colour_handler_cloud, "cloud", viewport_id_0);
    viewer.addPointCloud(in_plane, colour_handler_in_plane, "in_plane", viewport_id_1);
    viewer.addPointCloud(not_plane, colour_handler_not_plane, "not_plane", viewport_id_2);

    while (!viewer.wasStopped()) {
        viewer.spinOnce();
    }

    return (0);
}
