/*
 * 
        An external predicate defining a perceptual semantic attachment. 
        Hardcoded are the size of the grid.
        It uses a plane finder to find the plane that the grid is defined on automatically (defined in obstacles_pcl.cpp).
        The centre of the grid is defined to be the centroid of the points in the plane.
        
        
        --------------------------------------------------------------------
        
        Damien Jade Duff

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */      


#include "SWI-cpp.h"
#include <iostream>
#include <fstream>
#include <map>

/* Controls how much is printed to terminal. Uses stderr because swipl swallows stdout. */
const int verbosity=1;

/* How many points is considered an obstacle. */
const int num_points_for_obst=250;

/* How big the grid is that we are concerned with */
double grid_extent_x=1.0; // in metres
double grid_extent_y=1.0; // in metres
int num_grid_x=7;
int num_grid_y=7;

/* Variables derived from the above */
double minx=-grid_extent_x/2;
double miny=-grid_extent_y/2;
double stepx=grid_extent_x/num_grid_x;
double stepy=grid_extent_y/num_grid_y;


/* Just keep track of how many predicate calls are made */
int call_cntr=0;;

/* Variable that keeps track of whether a ground plane has been detected yet. */
bool ground_plane_found=false;

/* Unused variable that could potentially be used to reduce the perceptual computation by caching the results of the previous predicate call. */
std::map<std::pair<int,int>,bool> cached_obstacles;

/* External predicate defined in obstacles_pcl.cpp*/
extern bool find_ground_plane();
/* External predicate defined in obstacles_pcl.cpp*/    
extern int num_high_points(double xlim_low,double xlim_high,double ylim_low,double ylim_high);

/* Define the external predicate. */
PREDICATE(obstacleAt,2)
{
  
  int xcoord=A1;
  int ycoord=A2;
  if(verbosity>1)std::cerr<<"[obstacles_ext.cpp:obstacleAt]:obstacleAt external predicate entered."<<std::endl;
  
  //check if the ground plane has been found yet - if not, make the external call to find it.
  if(!ground_plane_found){
        if(verbosity>1)std::cerr<<"[obstacles_ext.cpp:obstacleAt]:Now doing PCL calculations"<<std::endl;
        ground_plane_found=find_ground_plane();
        if(verbosity>1)std::cerr<<"[obstacles_ext.cpp:obstacleAt]---found ground plane returns "<<ground_plane_found<<"---"<<std::endl;
  }

  //flag - will the obstacle be found?
  bool obstacle;
  
  if(!ground_plane_found){
      // If you are teaching your child about traffic lights, do you say "cross if you see green" or "don't cross if you see red"?
      // Here, we assume an obstacle if we can't find the plane. 
      obstacle=true;
  }
  else{
    
    // Calculate the limits of this particular grid square
    double xlim_low=minx+stepx*xcoord;
    double xlim_high=minx+stepx*(xcoord+1);
    
    double ylim_low=miny+stepy*ycoord;
    double ylim_high=miny+stepy*(ycoord+1);
    
    // Now send those limits to the external predicate that counts the number of points ABOVE that grid square.
    // Remember: uses the found plane and the centre of the grid is the centroid in the 3D world.
    if(verbosity>1)std::cerr<<"[obstacles_ext.cpp:obstacleAt]:Counting num points"<<std::endl;
    int num_possible_obstacle_points=num_high_points(xlim_low,xlim_high,ylim_low,ylim_high);
    if(verbosity>1)std::cerr<<"[obstacles_ext.cpp:obstacleAt]:Num points was "<<num_possible_obstacle_points<<std::endl;
    
    //threshold the points
    if (num_points_for_obst<num_possible_obstacle_points)obstacle=true;
    else obstacle=false;
  }
  
  if(verbosity>1)std::cerr<<"predicate.cpp:obstacleAt("<<xcoord<<","<<ycoord<<") ="<<obstacle<<" -- <"<<(call_cntr++)<<">"<<std::endl;
  if(verbosity==1){
    if(obstacle)
    std::cerr<<"obstacleAt("<<xcoord<<","<<ycoord<<")."<<std::endl;
    else
    std::cerr<<"%obstacleAt("<<xcoord<<","<<ycoord<<")."<<std::endl;
  }
  if (obstacle) return TRUE;
  else return FALSE;

}
    
