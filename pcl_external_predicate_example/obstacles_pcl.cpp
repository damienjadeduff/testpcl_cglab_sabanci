/*
 *
        Backend for obstacles_ext.so using PCL to do plane finding and point counting on a grid.
        Has a plane finder to find the plane that the grid is defined on automatically (defined in obstacles_pcl.cpp).
        The centre of the grid is defined to be the centroid of the points in the plane.

        --------------------------------------------------------------------

        Damien Jade Duff

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
#include <pcl/common/centroid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>

/* Hard-coded scene filename */
std::string datafilename = "pcl_sample_data/milk_cartoon_all_small_clorox.pcd";

/* Set this variable to get some visual output for each call to external predicate */
bool visualize = true;

int verbosity = 1;

/* How far above plane points must be before they are classified as a potential obstacle point.*/
double z_thresh = 0.01;

/* Keep track of whether the scene has been acquired yet. */
bool data_obtained = false;
bool plane_found = false;

/* Variable that contains the scene */
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
/* Variable that contains a KD-tree over the scene. */
pcl::KdTreeFLANN<pcl::PointXYZRGB> point_searcher;

/* Keep track of the centroid of the plane */
Eigen::Vector3f plane_centre;
/* Axes are calculated by using the vector from the closest point on the plane to the origin and the centroid (yaxis). */
Eigen::Vector3f xaxis;
/* X axis is calculated as orthogonal to y and z axes. */
Eigen::Vector3f yaxis;
/* Z axis is direction from closest point to origin, to origin (this is orthogonal to the plane). */
Eigen::Vector3f zaxis;

/* Plane coefficients are nx,ny,nz and d where nx,ny,nz is the normal vector of the plane and d is the distance from the origin to the cloest point on the plane (this is standard stuff).*/
pcl::ModelCoefficients plane_coefficients;

/* Some buffers */
std::vector<int> neighbour_indices(307200); //very big buffer, but we like certainty (307200=640*480)
std::vector<float> neighbour_distances(307200);

/* declare functions that will be defined below */
void obtain_data();
/* declare functions that will be defined below */
bool find_ground_plane();
/* declare functions that will be defined below */
int num_high_points(double xlim_low, double xlim_high, double ylim_low, double ylim_high);

/* Could acquire data from sensor, but here we just pick it up from a file */
void obtain_data()
{
    cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    if (pcl::io::loadPCDFile(datafilename, *cloud) < 0) {
        std::cerr << "[obstacles_pcl.cpp:obtain_data] Error loading data cloud. Will probably crash now." << std::endl;
    } else {
        point_searcher.setInputCloud(cloud);
        data_obtained = true;
    }
}

/* Use RANSAC to find the ground plane and canonical axes on it. */
bool find_ground_plane()
{

    if (!data_obtained)obtain_data();

    // will contain indices of points on the plane that is found with RANSAC
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

    //encapsulate the RANSAC algorithm (and similar SAC algorithms)
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;

    seg.setOptimizeCoefficients(true);//refines the pane after finding with RANSAC using basic nonlinear optimization

    seg.setModelType(pcl::SACMODEL_PLANE); //we are finding a pane
    seg.setMethodType(pcl::SAC_RANSAC); //the algorithm is RANSAC
    seg.setDistanceThreshold(0.01); //a point must be within 1cm of the plane to be counted as an inlier

    seg.setInputCloud(cloud); //the input dataset
    seg.segment(*inliers, plane_coefficients); //run the algorithm

    if (inliers->indices.size() == 0) {
        std::cerr << "[obstacles_pcl.cpp:find_ground_plane] Could not estimate a planar model for the given dataset." << std::endl;
        return false;
    } else {
        if (verbosity > 1)std::cerr << "[obstacles_pcl.cpp:find_ground_plane] Plane coefficients:" << plane_coefficients.values[0] << "," << plane_coefficients.values[1] << "," << plane_coefficients.values[2] << "," << plane_coefficients.values[3] << std::endl;
    }

    Eigen::Vector4f plane_centroid ; //would be much better to uniformly sample the plane first for really acutely angled planes
    pcl::compute3DCentroid<pcl::PointXYZRGB>(*cloud, inliers->indices, plane_centroid);

    // we would rather use a 3D vector than 4D.
    plane_centre = plane_centroid.head<3>();

    //Closest point on plane to origin.
    Eigen::Vector3f closest_point;
    closest_point[0] = -plane_coefficients.values[0] * plane_coefficients.values[3];
    closest_point[1] = -plane_coefficients.values[1] * plane_coefficients.values[3];
    closest_point[2] = -plane_coefficients.values[2] * plane_coefficients.values[3];

    //for the logic of this see the variable definitions at the top
    yaxis = plane_centre - closest_point;
    yaxis.normalize();

    //for the logic of this see the variable definitions at the top
    xaxis = yaxis.cross(plane_centre); //plane centre axis is perpendicular to plane
    xaxis.normalize();

    //for the logic of this see the variable definitions at the top
    zaxis = -closest_point.normalized();

    //visualize the point cloud and the plane and the centroid
    if (visualize) {

        pcl::visualization::PCLVisualizer viewer("Planar segmentation");
        viewer.setCameraPosition(0.0,-1.0,-2.0,0.0,0.0,1.0);

        int viewport_id_0;
        viewer.createViewPort(0, 0, 1.0, 1.0, viewport_id_0);

        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> colour_handler_cloud(cloud);

        viewer.addPointCloud(cloud, colour_handler_cloud, "cloud", viewport_id_0);
        viewer.addPlane(plane_coefficients, "plane", viewport_id_0);

        //convert "Eigen" vector to a PCL point
        pcl::PointXYZRGB plane_centre_pt;
        plane_centre_pt.x = plane_centre[0]; plane_centre_pt.y = plane_centre[1]; plane_centre_pt.z = plane_centre[2];
        pcl::PointXYZRGB closest_point_pt;
        closest_point_pt.x = closest_point[0]; closest_point_pt.y = closest_point[1]; closest_point_pt.z = closest_point[2];

        viewer.addSphere(plane_centre_pt, 0.03, 255, 0, 0, "centre", viewport_id_0);
        viewer.addSphere(closest_point_pt, 0.03, 0, 0, 255, "closest", viewport_id_0);

        //run the viewer untill someone presses the little X in the top corner...
        while (!viewer.wasStopped()) {
            viewer.spinOnce();
        }
      

    }

    if (inliers->indices.size() > 100) {
        plane_found = true;
        return true;
    } else return false; // not big enough inlier set (probably will never be hit)

}

/* Input: the limits of a grid square in the grid defined on the plane found above centred at the plane centre.
   Output: the number of points above that grid square (higher than z_thresh defined above).
 */
int num_high_points(double xlim_low, double xlim_high, double ylim_low, double ylim_high)
{
    if (!data_obtained)obtain_data();
    assert(plane_found);//the caller's responsibility to call this too.

    // find the centre of the grid square in the 2D plane coordinate system
    double x_centre = (xlim_low + xlim_high) / 2;
    double y_centre = (ylim_low + ylim_high) / 2;

    // the search radius will be the distance to a corner of the square from its centre.
    double search_rad = sqrt(pow(ylim_high - y_centre, 2) + pow(xlim_high - x_centre, 2)); //distance from corner to centre

    // convert the 2D coordinates of the plane centre to the 3D coordinates using the axes found above
    Eigen::Vector3f search_centre = plane_centre + x_centre * xaxis + y_centre * yaxis;
    // the same information but in a different format.
    pcl::PointXYZRGB search_centre_pt;
    search_centre_pt.x = search_centre[0];
    search_centre_pt.y = search_centre[1];
    search_centre_pt.z = search_centre[2];

    // find all the points close to that point. we will go through them and find the ones within the boundaries of the grid
    int num_found_neighbours = point_searcher.radiusSearch(search_centre_pt, search_rad, neighbour_indices, neighbour_distances);

    //keep track of obstacle points for visualization
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr viz_cloud1;
    if (visualize)viz_cloud1 = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);

    // Now go through all points close to the grid centre and find out if they are in the bounds of the grid and above the grid.
    // Then count them.
    int num_got = 0;
    for (int i = 0; i < neighbour_indices.size(); i++) {
        //The point that is close to the grid centre
        pcl::PointXYZRGB found = cloud->at(neighbour_indices.at(i));
        //The same point in a different format
        Eigen::Vector3f point;
        point[0] = found.x;
        point[1] = found.y;
        point[2] = found.z;
        //The vector from plane centroid to this point
        Eigen::Vector3f point_from_centre = point - plane_centre;
        //Find the coordinates in the plane-centred coordinate system of the point.
        double grid_x = point_from_centre.dot(xaxis);
        double grid_y = point_from_centre.dot(yaxis);
        double grid_z = point_from_centre.dot(zaxis);
        bool obst_pt = false;
        if (grid_x >= xlim_low)//the point is greater than the lower x limit
            if (grid_x < xlim_high)//the point is less than the higher x limit
                if (grid_y >= ylim_low)//the point is greater than the lower y limit
                    if (grid_y < ylim_high)//the point is lower than the higher y limit
                        if (grid_z > z_thresh) { // the point is above the plane

                            obst_pt = true;
                        }

        if (obst_pt)num_got++;

        if (visualize) {
            if (obst_pt)
                viz_cloud1->push_back(found);//visualize obstacle points
        }
    }


    if (visualize) {

        pcl::visualization::PCLVisualizer viewer("Point counting");
        viewer.setCameraPosition(0.0,-1.0,-2.0,0.0,0.0,1.0);

        int viewport_id_0;

        viewer.createViewPort(0, 0, 1.0, 1.0, viewport_id_0);

        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> colour_handler_cloud(cloud);
        viewer.addPointCloud(cloud, colour_handler_cloud, "cloud", viewport_id_0);

        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> red(viz_cloud1, 255, 0, 0);
        viewer.addPointCloud(viz_cloud1, red, "obstacle", viewport_id_0);

        //put grid corners into the point cloud frame of reference and draw the square
        Eigen::Vector3f bl_corner = plane_centre + xlim_low  * xaxis + ylim_low  * yaxis;
        Eigen::Vector3f tl_corner = plane_centre + xlim_low  * xaxis + ylim_high * yaxis;
        Eigen::Vector3f br_corner = plane_centre + xlim_high * xaxis + ylim_low  * yaxis;
        Eigen::Vector3f tr_corner = plane_centre + xlim_high * xaxis + ylim_high * yaxis;
        pcl::PointXYZRGB bl_corner_pt; bl_corner_pt.x = bl_corner[0]; bl_corner_pt.y = bl_corner[1]; bl_corner_pt.z = bl_corner[2];
        pcl::PointXYZRGB tl_corner_pt; tl_corner_pt.x = tl_corner[0]; tl_corner_pt.y = tl_corner[1]; tl_corner_pt.z = tl_corner[2];
        pcl::PointXYZRGB br_corner_pt; br_corner_pt.x = br_corner[0]; br_corner_pt.y = br_corner[1]; br_corner_pt.z = bl_corner[2];
        pcl::PointXYZRGB tr_corner_pt; tr_corner_pt.x = tr_corner[0]; tr_corner_pt.y = tr_corner[1]; tr_corner_pt.z = tr_corner[2];
        viewer.addLine(bl_corner_pt, tl_corner_pt, 0, 100, 255,  "gridleft",  viewport_id_0);
        viewer.addLine(bl_corner_pt, br_corner_pt, 100, 0, 255,  "gridbottom", viewport_id_0);
        viewer.addLine(tr_corner_pt, br_corner_pt, 0, 0, 255,    "gridright", viewport_id_0);
        viewer.addLine(tr_corner_pt, tl_corner_pt, 100, 100, 255, "gridtop",   viewport_id_0);

        while (!viewer.wasStopped()) {
            viewer.spinOnce();
        }
    
    }

    return num_got;

}
