####FindSWIPL.cmake acquired verbatim from TUM (Munich) PaRMa project.
####FindSWIPL.cmake acquired verbatim from http://code.cs.tum.edu/indefero/index.php//p/parma/source/tree/96/slope/cmake/FindSWIPL.cmake

# try first to use pkg-config --cflags 
find_package(PkgConfig)
pkg_check_modules(PC_LIBSWIPL swipl)
pkg_check_modules(PC_LIBPL pl)
set(LIBSWIPL_DEFINITIONS ${PC_LIBSWIPL_CFLAGS_OTHER})

# double check, using the pkg-config path as a hint
find_path(LIBSWIPL_INCLUDE_DIR SWI-Prolog.h
  HINTS ${PC_LIBSWIPL_INCLUDEDIR} ${PC_LIBSWIPL_INCLUDE_DIRS}
	${PC_LIBPL_INCLUDEDIR} ${PC_LIBPL_INCLUDE_DIRS}
  "/usr/lib/swi-prolog/include"
)

# same thing for library directory
find_library(LIBSWIPL_LIBRARY NAMES swipl pl
  HINTS ${PC_LIBSWIPL_LIBDIR} ${PC_LIBSWIPL_LIBRARY_DIRS}
        ${PC_LIBPL_LIBDIR} ${PC_LIBPL_LIBRARY_DIRS})

set(LIBSWIPL_LIBRARIES ${LIBSWIPL_LIBRARY} )
set(LIBSWIPL_INCLUDE_DIRS ${LIBSWIPL_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBXML2_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LIBSWIPL  DEFAULT_MSG
  LIBSWIPL_LIBRARY LIBSWIPL_INCLUDE_DIR)

mark_as_advanced(LIBSWIPL_INCLUDE_DIR LIBSWIPL_LIBRARY )
