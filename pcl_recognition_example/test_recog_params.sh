#!/bin/sh

echo
echo ---------------------- example usage:
echo ---------------------- $0 ~/Dropbox/IntroToPerception/box/box12.2x6.7x6.7cm.pcd ~/Dropbox/IntroToPerception/box/frame_simple.pcd
echo

MODEL_INPUT_PCD=$1 
SCENE_INPUT_PCD=$2 

MODEL_PASSTHROUGH_PCD=./pt_`basename $MODEL_INPUT_PCD`
SCENE_PASSTHROUGH_PCD=./pt_`basename $SCENE_INPUT_PCD`

MODEL_WITHNORMALS_PCD=./wn_`basename $MODEL_INPUT_PCD`
SCENE_WITHNORMALS_PCD=./wn_`basename $SCENE_INPUT_PCD`

MODEL_TRANSFORMED_PCD=./tr_`basename $MODEL_INPUT_PCD`
SCENE_TRANSFORMED_PCD=./tr_`basename $SCENE_INPUT_PCD`

MODEL_DOWNSAMPLED_PCD=./ds_`basename $MODEL_INPUT_PCD`
SCENE_DOWNSAMPLED_PCD=./ds_`basename $SCENE_INPUT_PCD`

MODEL_FEATURES_PCD=./fs_`basename $MODEL_INPUT_PCD`
SCENE_FEATURES_PCD=./fs_`basename $SCENE_INPUT_PCD`

NORMAL_ESTIMATION_RADIUS=0.01
DOWNSAMPLE_RADIUS=0.02
DESCRIPTOR_CALC_RADIUS=0.04
ZMIN_CUTOFF=-1.2
ZMAX_CUTOFF=1.2
ESTIMATOR=pcl_fpfh_estimation

PCL_BIN_DIR=~/software/pcl_trunk/build/bin
SAB_BIN_DIR=~/software/testpcl_cglab_sabanci

echo
echo ---------------------- Passthrough filter being applied
echo ---------------------- To view output type:
echo ---------------------- $PCL_BIN_DIR/pcl_viewer $SCENE_PASSTHROUGH_PCD
echo

$PCL_BIN_DIR/pcl_passthrough_filter -keep 0 -field z -min $ZMIN_CUTOFF -max $ZMAX_CUTOFF $MODEL_INPUT_PCD $MODEL_PASSTHROUGH_PCD
$PCL_BIN_DIR/pcl_passthrough_filter -keep 0 -field z -min $ZMIN_CUTOFF -max $ZMAX_CUTOFF $SCENE_INPUT_PCD $SCENE_PASSTHROUGH_PCD

echo
echo ---------------------- Normal estimation being applied
echo ---------------------- To view output type:
echo ---------------------- $PCL_BIN_DIR/pcl_viewer -normals 100 -normals_scale 0.01 $MODEL_WITHNORMALS_PCD
echo ---------------------- $PCL_BIN_DIR/pcl_viewer -normals 100 -normals_scale 0.01 $SCENE_WITHNORMALS_PCD
echo

$PCL_BIN_DIR/pcl_normal_estimation  -radius $NORMAL_ESTIMATION_RADIUS $SCENE_PASSTHROUGH_PCD $SCENE_WITHNORMALS_PCD
$PCL_BIN_DIR/pcl_normal_estimation  -radius $NORMAL_ESTIMATION_RADIUS $MODEL_PASSTHROUGH_PCD $MODEL_WITHNORMALS_PCD
# $PCL_BIN_DIR/pcl_normal_estimation  -radius $NORMAL_ESTIMATION_RADIUS $MODEL_INPUT_PCD $MODEL_WITHNORMALS_PCD

#$SAB_BIN_DIR/normal_estimation -viewpoint 0.0,20.0,0.0 -radius $NORMAL_ESTIMATION_RADIUS $SCENE_PASSTHROUGH_PCD $SCENE_WITHNORMALS_PCD
#$SAB_BIN_DIR/normal_estimation -viewpoint 0.0,20.0,0.0 -radius $NORMAL_ESTIMATION_RADIUS $MODEL_INPUT_PCD $MODEL_WITHNORMALS_PCD

echo
echo ---------------------- Transforming scene for better visualiztion later
echo ---------------------- To view output type:
echo ---------------------- $PCL_BIN_DIR/pcl_viewer -normals 100 -normals_scale 0.01 $SCENE_TRANSFORMED_PCD
echo

# uncomment to move scene around a bit $PCL_BIN_DIR/pcl_transform_point_cloud -trans 0.0,0.0,-0.8 $SCENE_WITHNORMALS_PCD $SCENE_TRANSFORMED_PCD
cp $MODEL_WITHNORMALS_PCD $MODEL_TRANSFORMED_PCD 
cp $SCENE_WITHNORMALS_PCD $SCENE_TRANSFORMED_PCD # doesn't move scene

echo
echo ---------------------- Uniform sampling being applied
echo ---------------------- To view output type:
echo ---------------------- $PCL_BIN_DIR/pcl_viewer $MODEL_DOWNSAMPLED_PCD
echo ---------------------- $PCL_BIN_DIR/pcl_viewer $SCENE_DOWNSAMPLED_PCD
echo

$PCL_BIN_DIR/pcl_uniform_sampling -radius ${DOWNSAMPLE_RADIUS} $MODEL_TRANSFORMED_PCD $MODEL_DOWNSAMPLED_PCD
$PCL_BIN_DIR/pcl_uniform_sampling -radius ${DOWNSAMPLE_RADIUS} $SCENE_TRANSFORMED_PCD $SCENE_DOWNSAMPLED_PCD

echo
echo ---------------------- Feature extraction being applied
echo ---------------------- To view output type:
echo ---------------------- $PCL_BIN_DIR/pcl_viewer $MODEL_FEATURES_PCD $SCENE_FEATURES_PCD
echo ---------------------- Use "l" to see the available visualizable colours, and press the appropriate number to visualize e.g. the descriptor, x normal, etc.
echo

$PCL_BIN_DIR/$ESTIMATOR -radius $DESCRIPTOR_CALC_RADIUS $MODEL_DOWNSAMPLED_PCD $MODEL_FEATURES_PCD 
$PCL_BIN_DIR/$ESTIMATOR -radius $DESCRIPTOR_CALC_RADIUS $SCENE_DOWNSAMPLED_PCD $SCENE_FEATURES_PCD

#$PCL_BIN_DIR/pcl_viewer $MODEL_FEATURES_PCD $SCENE_FEATURES_PCD 
