PCL tutorial programs and external predicate example
====================================================

This repository contains examples of:

- Plane-finding in point clouds (e.g. Kinect data) using PCL.
- External predicates with PCL (interfacing into SWI Prolog or CCalc) in an object detection scenario.
- Object recognition with PCL.

Before you can do any PCL (Point Cloud Library) tutorials, the first step is to obtain PCL. 

Install PCL
=====================

In the following demonstration, everything will install into `~/software` - if you wish to install the software elsewhere, just change that part in the following instructions (and `CMakeLists.txt` in the test project) to ensure everything is in order. 

For more info on how to get PCL: [http://pointclouds.org/downloads/source.html](http://pointclouds.org/downloads/source.html) (but you should be able to follow the instructions below).

Step 1 - Get dependencies
------
The following has been tested on *(k)ubuntu 12.04*. Very similar procedures should be possible with other versions of ubuntu.

First of all, get the dependencies (copy and paste into a shell):

    sudo apt-get install libqhull-dev libvtk5-qt4-dev libflann-dev libvtk5-dev libusb-1.0-0-dev libeigen3-dev libboost-all-dev build-essential cmake-curses-gui

- *libqhull* can be skipped if you disable the surface modules - it is for finding hulls around points.
- *vtk* is for visualization - essential.
- *libflann* is a fast nearest-neighbour search library - essential.
- *libusb* is for IO - a dependency.
- *Eigen* is a linear algebra and matrix manipulation library for C++ - essential.
- *build-essential* contains GCC compiler, cmake, etc - essential.
- *cmake-curses-gui* is the gui for cmake - only required if you want to disable PCL modules easily (and the external predicate example below).

Now set up your software directory and download PCL source code:

    mkdir ~/software
    cd ~/software
    git clone https://github.com/PointCloudLibrary/pcl.git pcl_trunk # get the codez

Now run ``cmake`` to configure the build - everything will be built in this ``build`` directory.

    cd pcl_trunk
    mkdir build # where stuff will be compiled to
    cd build
    cmake ..

Note: Optionally, use `ccmake` instead of `cmake` if you want to disable PCL modules interactively (for example, if one of these modules is not building).

Now build PCL (this can take between 20 minutes and 3 hours depending on your computer):

    make
    
If you have multiple cores, make use of them like this, instead:

    make -j 3 # use 3 cores

Normally we are instructed to run ``make install`` but we won't do that because we will just link to PCL where it was built (and I would recommend ``checkinstall`` rather than ``make install`` anyway).

Note: Optionally, if you want to access the kinect, get the ``openni-dev`` and ``ps-engine`` packages from one of the ROS repositories (ask me for help).

Step 2 - Compile a test program
------

Here we will download a short program and try to compile it to ensure that PCL is installed.

Download the program:

    cd ~/software
    git clone https://bitbucket.org/damienjadeduff/testpcl_cglab_sabanci.git testpcl_cglab_sabanci

Make a build directory and compile the test program:
    
    cd ~/software/testpcl_cglab_sabanci
    mkdir build
    cd build
    cmake ..
    make
    
**NOTE:** If you have put PCL into a different location than `~/software`, you will need to edit the file `CMakeLists.txt` in the `testpcl_cglab_sabanci` directory and set `PCL_DIR` to point to the appropriate directory.


Plane finding
=============

The plane finder should be built automatically and there is some test data provided (via PCL). To test the plane finder (after compiling the system as described above), do:

    cd ~/software/testpcl_cglab_sabanci
    ./planefind_test ./pcl_sample_data/milk_cartoon_all_small_clorox.pcd 
    
Note that the external predicate example makes use of a plane-finder very similar to this, so this might be a good place to start understanding the more complicated code found in the external predicate example.


External predicate
==================

Building
--------

The following sections assume you have downloaded the test program (`testpcl_cglab_sabanci`) as described just above.

Do the following to get a GUI from which you can turn on `WITH_OBSTACLES_SWIPL` and `WITH_OBSTACLES_PCL` to build the external predicate.

    cd ~/software/testpcl_cglab_sabanci
    ccmake ..
    
After enabling them, call `make` again.

SWI Prolog
----------
    
You will need `SWI Prolog` installed on your system for this to work (you probably do already if you are following this tutorial). 

For the rest of this example, make sure you are in the following directory (as the example picks up sample files from a sub-directory):

    cd ~/software/testpcl_cglab_sabanci/

To test that the external predicate is working in `SWI Prolog`, start the Prolog interpreter, include the external predicate and try it out:

Starting prolog will be something like this:

    swipl
    
Once within the interpreter, load the library and test it:

    use_foreign_library('libobstacles_ext.so').
    obstacleAt(3,3).
    
CCalc
-----
    
You can also test that the external predicates are working within `CCalc`. This will differ from system to system. On my system I get it by running:

    swipl -f /opt/software/ccalc/ccalc.pl
    
Now load the external predicates and run the multi-robot planning test plan by calling the following commands from within `CCalc`:

    use_foreign_library('libobstacles_ext.so').
    loadf 'pcl_external_predicate_example/problem.ccalc'.
    query 0.
    
CCalc should find a plan much like the following:

    ACTIONS:  move(r1,up)  move(r2,left)  move(r3,left)  
    ACTIONS:  move(r2,up)  move(r3,down)  
    ACTIONS:  move(r2,up)  move(r3,left)  
    ACTIONS:  move(r1,up)  move(r2,up)  move(r3,left)  
    ACTIONS:  move(r1,up)  move(r2,up)  move(r3,left)  
    ACTIONS:  move(r1,right)  move(r2,up)  
    ACTIONS:  move(r1,right)  move(r2,left)  move(r3,down)  
    ACTIONS:  move(r1,up)  move(r2,left)  
    ACTIONS:  move(r1,right)  move(r2,left)  move(r3,left)  
    ACTIONS:  move(r1,right)  move(r2,down)  move(r3,down)  

The source code for this example can be found in the `pcl_external_predicate_example` directory.

Recognition
===========

The recognition example is also built automatically (if PCL is found on your system) and has hard-coded parameters. To run it on the sample data (there are two sets of sample data) do:

    cd ~/software/testpcl_cglab_sabanci
    ./recog_test -c ./pcl_sample_data/milk.pcd ./pcl_sample_data/milk_cartoon_all_small_clorox.pcd 
    ./recog_test -c ./pcl_sample_data/CoffeeboxSource.pcd ./pcl_sample_data/CoffeeboxFrameTarget.pcd 
    
The program will save some intermediate files for you to inspect after the normal calculation and downsampling stages.

If you need to tune for your problem, use the script `test_recog_params.sh`, which will allow you to tune up to the feature extraction phase. It uses `PCL`'s own command line tools to process input files. Edit the parameters in the file to get different results. The script leaves behind intermediate files for you to inspect and prints out convenient commands for inspecting them.

    cd ~/software/testpcl_cglab_sabanci
    ./pcl_recognition_example/test_recog_params.sh ./pcl_sample_data/CoffeeboxSource.pcd ./pcl_sample_data/CoffeeboxFrameTarget.pcd 
    
To get a segmented model to recognize in target scenes you can either make a PLY with a tool like Blender and convert it to PCL format using the PCL tool `pcl_ply2pcd` (which should be built in `~/software/pcl_trunk/build/bin/`) OR you can grab views of your model by putting it on a flat surface and using the `pcl_pcd_select_object_plane` tool. 

In order to do the latter, run the `pcl_pcd_select_object_plane` tool on your target file as follows, and ``SHIFT-left-click`` on your object to be segmented using the ground plane as a reference:

    cd ~/software/testpcl_cglab_sabanci
    ~/software/pcl_trunk/build/bin/pcl_pcd_select_object_plane ./pcl_sample_data/milk_cartoon_all_small_clorox.pcd segmented_object.pcd segmented_plane.pcd
    
If the tool crashes on "organised" data (such as comes from the Kinect) you can either apply a passthrough filter (`pcl_passthrough_filter`) configured to save the point cloud unorganised (run `pcl_passthrough_filter -h` to see instructions for the filter), or patch PCL as follows to not crash on organised data:

    cd ~/software/pcl_trunk
    wget https://github.com/damienjadeduff/pcl/commit/27cbeaa9262387ad7b7e53943f40c4b4583fed3f.patch
    patch -p1 <27cbeaa9262387ad7b7e53943f40c4b4583fed3f.patch 
    cd build
    make
    
