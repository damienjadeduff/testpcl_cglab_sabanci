//simplified from http://pointclouds.org/documentation/tutorials/correspondence_grouping.php

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/pfh.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/board.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/impl/cg/hough_3d.hpp>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>


std::string model_filename_;
std::string scene_filename_;


bool show_correspondences_(false);


void
parseCommandLine(int argc, char* argv[])
{

    std::vector<int> filenames;
    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
    if (filenames.size() != 2) {
        std::cout << "Filenames missing.\n";
        exit(-1);
    }

    model_filename_ = argv[filenames[0]];
    scene_filename_ = argv[filenames[1]];

    if (pcl::console::find_switch(argc, argv, "-c")) {
        show_correspondences_ = true;
    }


}

int
main(int argc, char* argv[])
{
    parseCommandLine(argc, argv);

    //setup variables for use
    //first list is the model cloud and all derived clouds
    //the model cloud:
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr model(new pcl::PointCloud<pcl::PointXYZRGB> ());
    //normals calculated from model cloud
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr model_normals(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
    //keypoints extracted from model cloud (with normals)
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr model_keypoints(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
    //local shape descriptors calculated from model cloud (calculated at the keypoints)
    pcl::PointCloud<pcl::FPFHSignature33>::Ptr model_descriptors(new pcl::PointCloud<pcl::FPFHSignature33> ());;

    //second list is related to the target scene and all derived clouds
    //the scene cloud:
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr scene(new pcl::PointCloud<pcl::PointXYZRGB> ());
    //normals calculated from scene cloud
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr scene_normals(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
    //keypoints extracted from scene cloud (with normals)
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr scene_keypoints(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());
    //local shape descriptors calculated from scene cloud (calculated at the keypoints)
    pcl::PointCloud<pcl::FPFHSignature33>::Ptr scene_descriptors(new pcl::PointCloud<pcl::FPFHSignature33> ());

    //
    //  Load clouds
    //
    std::cout << "Loading model cloud" << std::endl;
    if (pcl::io::loadPCDFile(model_filename_, *model) < 0) {
        std::cout << "Error loading model cloud." << std::endl;
        return (-1);
    }
    std::cout << "Loading scene cloud" << std::endl;
    if (pcl::io::loadPCDFile(scene_filename_, *scene) < 0) {
        std::cout << "Error loading scene cloud." << std::endl;
        return (-1);
    }

    //fill in XYZRGB data into the normal clouds - we will presently calculate the normals there too
    pcl::copyPointCloud(*model, *model_normals);
    pcl::copyPointCloud(*scene, *scene_normals);

    //
    //  Compute Normals
    //
    std::cout << "Normal computation..." << std::endl;
    pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> norm_est;
    //norm_est.setKSearch(10); //another possibility
    norm_est.setRadiusSearch(0.01);

    //calculate for the model cloud
    norm_est.setInputCloud(model);
    norm_est.compute(*model_normals);

    //do the same for the scene cloud
    norm_est.setInputCloud(scene);
    norm_est.compute(*scene_normals);

    //save normal clouds computed so far for debugging
    pcl::io::savePCDFile("model_normals_tmp.pcd", *model_normals) ;
    pcl::io::savePCDFile("scene_normals_tmp.pcd", *scene_normals) ;

    //
    //  Downsample Clouds to Extract keypoints (approaches for extracting informative keypoints are usually better)
    //
    pcl::PointCloud<int> sampled_indices; //the indices of the keypoints, indexed into original cloud

    std::cout << "Uniform sample model..." << std::endl;
    pcl::UniformSampling<pcl::PointXYZRGBNormal> uniform_sampling;
    uniform_sampling.setInputCloud(model_normals);
    uniform_sampling.setRadiusSearch(0.01f);
    uniform_sampling.compute(sampled_indices);
    pcl::copyPointCloud(*model_normals, sampled_indices.points, *model_keypoints);
    std::cout << "Model total points: " << model->size() << "; Selected Keypoints: " << model_keypoints->size() << std::endl;

    std::cout << "Uniform sample scene..." << std::endl;
    uniform_sampling.setInputCloud(scene_normals);
    uniform_sampling.setRadiusSearch(0.01f);
    uniform_sampling.compute(sampled_indices);
    pcl::copyPointCloud(*scene_normals, sampled_indices.points, *scene_keypoints);
    std::cout << "Scene total points: " << scene->size() << "; Selected Keypoints: " << scene_keypoints->size() << std::endl;

    //save downsampled clouds computed so far for debugging
    pcl::io::savePCDFile("model_keypoints_tmp.pcd", *model_keypoints) ;
    pcl::io::savePCDFile("scene_keypoints_tmp.pcd", *scene_keypoints) ;
    ////
    //  Compute Descriptor for keypoints
    //
    std::cout << "FPFH estimation..." << std::endl;
    pcl::FPFHEstimation<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal, pcl::FPFHSignature33> descr_est;
    descr_est.setRadiusSearch(0.06f);

    //extract descriptors of model file
    descr_est.setInputCloud(model_keypoints);
    descr_est.setInputNormals(model_keypoints);

    std::cout << "FPFH for model..." << std::endl;
    descr_est.compute(*model_descriptors);

    //and now same for scene
    descr_est.setInputCloud(scene_keypoints);
    descr_est.setInputNormals(scene_keypoints);

    std::cout << "FPFH for scene..." << std::endl;
    descr_est.compute(*scene_descriptors);

    //save the descriptors for debugging (alas, lack xyz info at this point, so may not be of much use)
    pcl::io::savePCDFile("model_features_tmp.pcd", *model_descriptors) ;
    pcl::io::savePCDFile("scene_features_tmp.pcd", *scene_descriptors) ;

    //
    //  Find Model-Scene Correspondences with KdTree
    //
    std::cout << "Find correspondences..." << std::endl;
    pcl::CorrespondencesPtr model_scene_corrs(new pcl::Correspondences());

    pcl::KdTreeFLANN<pcl::FPFHSignature33> match_search;
    match_search.setInputCloud(model_descriptors);

    //  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
    for (size_t i = 0; i < scene_descriptors->size(); ++i) {
        std::vector<int> neigh_indices(1);
        std::vector<float> neigh_sqr_dists(1);
        if (!pcl_isfinite(scene_descriptors->at(i).histogram[0])) { //skipping NaNs
            continue;
        }
        int found_neighs = match_search.nearestKSearch(scene_descriptors->at(i), 1, neigh_indices, neigh_sqr_dists);
//         int cntr=0;
        //we asked for one neighbour so if a neighbour was found (should be) we will add that.
        //note: ALL matches will be included - we will use Hough to find the best ones - could be smarter
        if (found_neighs == 1) {  //  we could use distance to determine what is a good match if we are being clever, but distance depends on scale of feature
            pcl::Correspondence corr(neigh_indices[0], static_cast<int>(i), neigh_sqr_dists[0]);
            model_scene_corrs->push_back(corr);
//             if(cntr%100==0)std::cout<<"dist:"<< neigh_sqr_dists[0] <<std::endl;
//             cntr++;
        }
    }
    std::cout << "Correspondences found: " << model_scene_corrs->size() << std::endl;

    //
    //  Cluster correspences
    //
    // Note that there can be more than 1 solution; that is why we are using a vector.
    std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > rototranslations; //where the solution transforms will be stored
    std::vector<pcl::Correspondences> clustered_corrs; //where the solution correspondences will be stored


    pcl::PointCloud<pcl::ReferenceFrame>::Ptr model_rf(new pcl::PointCloud<pcl::ReferenceFrame> ());
    pcl::PointCloud<pcl::ReferenceFrame>::Ptr scene_rf(new pcl::PointCloud<pcl::ReferenceFrame> ());

    pcl::BOARDLocalReferenceFrameEstimation<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal, pcl::ReferenceFrame> rf_est;
    rf_est.setFindHoles(true);
    rf_est.setRadiusSearch(0.03f);

    rf_est.setInputCloud(model_keypoints);
    rf_est.setInputNormals(model_keypoints);
    rf_est.compute(*model_rf);

    rf_est.setInputCloud(scene_keypoints);
    rf_est.setInputNormals(scene_keypoints);
    rf_est.compute(*scene_rf);

    //  Clustering
    pcl::Hough3DGrouping<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal, pcl::ReferenceFrame, pcl::ReferenceFrame> clusterer;
    clusterer.setHoughBinSize(0.03f);
    clusterer.setHoughThreshold(25.0f);
    clusterer.setUseInterpolation(true);
    clusterer.setUseDistanceWeight(false);

    clusterer.setInputCloud(model_keypoints);
    clusterer.setInputRf(model_rf);
    clusterer.setSceneCloud(scene_keypoints);
    clusterer.setSceneRf(scene_rf);
    clusterer.setModelSceneCorrespondences(model_scene_corrs);

    clusterer.recognize(rototranslations, clustered_corrs);


    //
    //  Output results
    //
    std::cout << "Model instances found: " << rototranslations.size() << std::endl;
    for (size_t i = 0; i < rototranslations.size(); ++i) {
        std::cout << "\n    Instance " << i + 1 << ":" << std::endl;
        std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size() << std::endl;

        // Print the rotation matrix and translation vector
        Eigen::Matrix3f rotation = rototranslations[i].block<3, 3>(0, 0);
        Eigen::Vector3f translation = rototranslations[i].block<3, 1>(0, 3);

        printf("\n");
        printf("            | %6.3f %6.3f %6.3f | \n", rotation(0, 0), rotation(0, 1), rotation(0, 2));
        printf("        R = | %6.3f %6.3f %6.3f | \n", rotation(1, 0), rotation(1, 1), rotation(1, 2));
        printf("            | %6.3f %6.3f %6.3f | \n", rotation(2, 0), rotation(2, 1), rotation(2, 2));
        printf("\n");
        printf("        t = < %0.3f, %0.3f, %0.3f >\n", translation(0), translation(1), translation(2));
    }

    //
    //  Visualization
    //
    pcl::visualization::PCLVisualizer viewer("Correspondence Grouping");
    viewer.setCameraPosition(0.0, -1.0, -2.0, 0.0, 0.0, 1.0);
    viewer.addPointCloud(scene, "scene_cloud");

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr off_scene_model_keypoints(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());

    if (show_correspondences_) {
        //  We are translating the model so that it doesn't end in the middle of the scene representation
        pcl::transformPointCloud(*model_keypoints, *off_scene_model_keypoints, Eigen::Vector3f(-1, 0, 0), Eigen::Quaternionf(1, 0, 0, 0));

        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> off_scene_model_color_handler(off_scene_model_keypoints, 255, 255, 128);
        viewer.addPointCloud(off_scene_model_keypoints, off_scene_model_color_handler, "off_scene_model");
    }


    for (size_t i = 0; i < rototranslations.size(); ++i) {
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr rotated_model(new pcl::PointCloud<pcl::PointXYZRGBNormal> ());

        pcl::transformPointCloud(*model_keypoints, *rotated_model, rototranslations[i]);

        std::stringstream ss_cloud;
        ss_cloud << "instance" << i;

        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> rotated_model_color_handler(rotated_model, 255, 0, 0);
        viewer.addPointCloud(rotated_model, rotated_model_color_handler, ss_cloud.str());

        if (show_correspondences_) {
            for (size_t j = 0; j < clustered_corrs[i].size(); ++j) {

                std::stringstream ss_line;
                ss_line << "correspondence_line" << i << "_" << j;
                pcl::PointXYZRGBNormal& model_point = off_scene_model_keypoints->at(clustered_corrs[i][j].index_query);
                pcl::PointXYZRGBNormal& scene_point = scene_keypoints->at(clustered_corrs[i][j].index_match);

                //  We are drawing a line for each pair of clustered correspondences found between the model and the scene
                viewer.addLine<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal> (model_point, scene_point, 0, 255, 0, ss_line.str());
            }
        }
    }

    while (!viewer.wasStopped()) {
        viewer.spinOnce();
    }

    return (0);
}

